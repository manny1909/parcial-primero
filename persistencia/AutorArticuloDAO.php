<?php
class AutorArticuloDAO{
    private $idAutorArticulo;
    private $nombre;
    private $apellido;
    
    function AutorDAO ($pIdAutorArticulo, $pNombre, $pApellido) {
        $this -> idAutorArticulo = $pIdAutorArticulo;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
    }
    
    function consultar () {
        return "select nombre, apellido
                from AutorArticulo
                where idAutorArticulo = '" . $this -> idAutorArticulo . "'";
    }
    
    function crear () {
        return "insert into AutorArticulo (nombre, apellido)
                values ('" . $this -> nombre . "', '" . $this -> apellido . "')";                
    }
    
    function consultarTodos () {
        return "select idAutorArticulo, nombre, apellido
                from AutorArticulo";
    }
    
    function editar () {
        return "update AutorArticulo 
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "'
                where idAutorArticulo = '" . $this -> idAutorArticulo . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina) {
        return "select idAutorArticulo, nombre, apellido
                from AutorArticulo
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;
    }
    
    function consultarTotalRegistros () {
        return "select count(idAutor)
                from AutorArticulo";
    }
    
}

?>