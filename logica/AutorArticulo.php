<?php
require "persistencia/AutorArticuloDAO.php";

class AutorArticulo{
    private $idAutorArticulo;
    private $nombre;
    private $apellido;
    private $conexion;
    private $autorarticuloDAO;
    

    
    /**
     * @return string
     */
    public function getIdAutorArticulo()
    {
        return $this->idAutorArticulo;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    function Autor ($pIdAutorArticulo="", $pNombre="", $pApellido="") {
        $this -> idAutorArticulo = $pIdAutorArticulo;
        $this -> nombre = $pNombre;
        $this -> apellido = $pApellido;
        $this -> conexion = new Conexion();
        $this -> autorarticuloDAO = new AutorarticuloDAO($pIdAutorArticulo, $pNombre, $pApellido);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> autorarticuloDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> autorarticuloDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> autorarticuloDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $autores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($autores, new AutorArticulo($resultado[0], $resultado[1], $resultado[2]));
        }
        return $autores;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> autorarticuloDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> autorarticuloDAO -> consultarPorPagina($cantidad, $pagina));
        $this -> conexion -> cerrar();
        $autores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($autores, new AutorArticulo($resultado[0], $resultado[1], $resultado[2]));
        }
        return $autores;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> autorarticuloDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    
}


?>